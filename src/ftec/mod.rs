use log::*;
use crate::{
    fs_util::open_write,
    range_error::RangeError,
    util::PathContainer,
};
use regex::Regex;
use std::{
    borrow::Cow,
    convert::TryFrom,
    io::{
        self,
        ErrorKind,
    },
    fs,
    ops::Range,
    path::{
        Path,
        PathBuf,
    },
    str::FromStr,
};
use mcoffin_option_ext::*;

mod leds;

pub use leds::FanatecLeds;

pub const FANATEC_VENDOR_ID: usize = 0x0EB7;
const DRIVER_PATH: &str = "/sys/module/hid_fanatec/drivers/hid:ftec_csl_elite";

#[derive(Debug, thiserror::Error)]
enum IdentifiersError<'a> {
    #[error("Invalid name: {0}")]
    InvalidName(Cow<'a, str>),
    #[error("Invalid id: {0}")]
    InvalidId(<usize as FromStr>::Err),
}

impl<'a> IdentifiersError<'a> {
    #[inline(always)]
    fn invalid_name(name: &'a str) -> Self {
        IdentifiersError::InvalidName(Cow::Borrowed(name))
    }
}

fn identifiers(name: &str) -> Result<(usize, usize), IdentifiersError<'_>> {
    lazy_static! {
        static ref NODE_PATTERN: Regex = Regex::new(r"^([0-9A-F]+):([0-9A-F]{4}):([0-9A-F]{4})(.([0-9A-F]{4}))?$").unwrap();
    }
    let captures = NODE_PATTERN.captures(name)
        .map_or_else(|| Err(IdentifiersError::invalid_name(name)), Ok)?;
    let parse_id = move |idx: usize| {
        usize::from_str_radix(&captures[idx], 16)
            .map_err(IdentifiersError::InvalidId)
    };
    Ok((parse_id(2)?, parse_id(3)?))
}

pub const LOAD_CELL_RANGE: Range<u8> = 1..11;

pub trait ComponentKind: Default {
    fn from_id(vendor_id: usize, device_id: usize) -> Option<Self>;

    #[inline(always)]
    fn from_id_or_unknown(vendor_id: usize, device_id: usize) -> Self {
        Self::from_id(vendor_id, device_id).or_default()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum WheelKind {
    Unknown,
    CSW2_5,
}

impl Default for WheelKind {
    #[inline(always)]
    fn default() -> Self {
        WheelKind::Unknown
    }
}

impl ComponentKind for WheelKind {
    #[inline(always)]
    fn from_id(vendor_id: usize, device_id: usize) -> Option<Self> {
        if vendor_id != FANATEC_VENDOR_ID {
            return None;
        }
        match device_id {
            0x0004 => Some(WheelKind::CSW2_5),
            _ => None,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum PedalsKind {
    Unknown,
    CSLElite,
    ClubSportV3,
}

impl Default for PedalsKind {
    #[inline(always)]
    fn default() -> Self {
        PedalsKind::Unknown
    }
}

impl ComponentKind for PedalsKind {
    #[inline(always)]
    fn from_id(vendor_id: usize, device_id: usize) -> Option<Self> {
        if vendor_id != FANATEC_VENDOR_ID {
            return None;
        }
        match device_id {
            0x6204 => Some(PedalsKind::CSLElite),
            0x183b => Some(PedalsKind::ClubSportV3),
            _ => None,
        }
    }
}

pub struct FanatecPedals<P: AsRef<Path>> {
    path: P,
    kind: PedalsKind,
}

impl<P: AsRef<Path>> FanatecPedals<P> {
    #[inline(always)]
    fn map_path<F, Ret>(self, f: F) -> FanatecPedals<Ret>
    where
        F: FnOnce(P) -> Ret,
        Ret: AsRef<Path>,
    {
        FanatecPedals {
            path: f(self.path),
            kind: self.kind,
        }
    }
}

impl FanatecPedals<PathBuf> {
    pub fn new_autodetect() -> io::Result<Self> {
        let entries = fs::read_dir(DRIVER_PATH)?;
        let mut ret: Option<FanatecPedals<PathBuf>> = None;
        for entry in entries {
            let entry = entry?;
            let name = entry.file_name();
            let name = name.to_string_lossy();
            trace!("Checking {} for pedals", &name);
            match identifiers(&name) {
                Ok((vendor_id, device_id)) => {
                    trace!("{} is pedals", &name);
                    debug!("Found pedals: {:x}:{:x}", vendor_id, device_id);
                    if vendor_id != FANATEC_VENDOR_ID {
                        continue;
                    }
                    let pedals = FanatecPedals {
                        kind: PedalsKind::from_id(vendor_id, device_id).or_default(),
                        path: entry.path(),
                    };
                    if pedals.kind() != PedalsKind::Unknown {
                        return Ok(pedals);
                    } else {
                        ret = Some(pedals);
                    }
                },
                Err(e) => {
                    trace!("{} is not pedals: {}", &name, e);
                    continue;
                },
            }
        }
        ret.map_or_else(|| {
            let msg = format!("No matching pedals found in: {}", DRIVER_PATH);
            Err(io::Error::new(ErrorKind::NotFound, msg))
        }, Ok)
    }
}

impl<'a> From<&'a Path> for FanatecPedals<&'a Path> {
    #[inline(always)]
    fn from(path: &'a Path) -> Self {
        FanatecPedals {
            kind: PedalsKind::Unknown, // TODO: derive from path?
            path,
        }
    }
}

impl<'a> FanatecPedals<Cow<'a, Path>> {
    pub fn new<P: Into<Option<&'a Path>>>(path: P) -> io::Result<Self> {
        Self::try_from(path.into())
    }
}

impl<'a> TryFrom<Option<&'a Path>> for FanatecPedals<Cow<'a, Path>> {
    type Error = io::Error;
    fn try_from(path: Option<&'a Path>) -> Result<Self, Self::Error> {
        path.map_or_else(|| {
            let ret = FanatecPedals::new_autodetect()?;
            Ok(ret.map_path(Cow::Owned))
        }, |path| {
            let ret = FanatecPedals::from(path)
                .map_path(Cow::Borrowed);
            Ok(ret)
        })
    }
}

impl<P: AsRef<Path>> FanatecPedals<P> {
    #[inline(always)]
    pub fn kind(&self) -> PedalsKind {
        self.kind
    }

    #[inline(always)]
    pub fn path(&self) -> &Path {
        self.path.as_ref()
    }

    fn subpath<Ext: AsRef<Path>>(&self, p: Ext) -> PathBuf {
        use std::iter;
        let mut ret = PathBuf::from(self.path());
        ret.extend(iter::once(p));
        ret
    }

    pub fn set_load_cell(&self, value: u8) -> io::Result<()> {
        use io::Write;
        if !LOAD_CELL_RANGE.contains(&value) {
            return Err(RangeError::new(LOAD_CELL_RANGE, value).into());
        }
        let load_path = self.subpath("load");
        debug!("load_path = {}", load_path.display());
        let mut f = open_write(load_path)?;
        write!(&mut f, "{}", value)
    }
}

pub struct FanatecWheel<P: AsRef<Path>> {
    kind: WheelKind,
    path: P,
}

impl<P: AsRef<Path>> PathContainer for FanatecWheel<P> {
    #[inline(always)]
    fn path(&self) -> &Path {
        self.path.as_ref()
    }
}

impl<P: AsRef<Path>> FanatecWheel<P> {
    #[inline(always)]
    fn map_path<F, Ret>(self, f: F) -> FanatecWheel<Ret>
    where
        F: FnOnce(P) -> Ret,
        Ret: AsRef<Path>,
    {
        FanatecWheel {
            kind: self.kind,
            path: f(self.path),
        }
    }

    #[inline(always)]
    fn new_unknown(path: P) -> Self {
        FanatecWheel {
            kind: WheelKind::Unknown,
            path,
        }
    }
    #[inline(always)]
    pub fn kind(&self) -> WheelKind {
        self.kind
    }

    pub fn set_display_number(&self, value: u32) -> io::Result<()> {
        use io::Write;
        let display_path = self.subitem("display");
        debug!("display_path = {}", display_path.display());
        open_write(display_path)
            .and_then(|mut f| write!(&mut f, "{}", value))
    }

    pub fn leds(&self) -> FanatecLeds<&Path> {
        FanatecLeds::new(self.path())
    }
}

impl FanatecWheel<PathBuf> {
    pub fn new_autodetect() -> io::Result<Self> {
        let entries = fs::read_dir(DRIVER_PATH)?
            .filter_map(Result::ok);

        for entry in entries {
            let name = entry.file_name();
            let name = name.to_string_lossy();
            trace!("Checking {} for pedals", &name);
            match identifiers(&name).map(|(vendor, device)| WheelKind::from_id_or_unknown(vendor, device)) {
                Ok(WheelKind::Unknown) => {
                    trace!("{} is not a known wheel", &name);
                },
                Ok(kind) => {
                    trace!("{} is a wheel of kind: {:?}", &name, &kind);
                    return Ok(FanatecWheel {
                        kind,
                        path: entry.path(),
                    });
                },
                Err(e) => {
                    trace!("{} is not pedals: {}", &name, e);
                    continue;
                },
            }
        }
        let msg = format!("Failed to find matching wheel in {}", DRIVER_PATH);
        Err(io::Error::new(ErrorKind::NotFound, msg))
    }
}

impl<'a> TryFrom<Option<&'a Path>> for FanatecWheel<Cow<'a, Path>> {
    type Error = io::Error;
    fn try_from(path: Option<&'a Path>) -> Result<Self, Self::Error> {
        path
            .map(|p| Self::new_unknown(Cow::Borrowed(p)))
            .map_or_else(|| {
                let ret = FanatecWheel::new_autodetect()?;
                Ok(ret.map_path(Cow::Owned))
            }, Ok)
    }
}

impl<'a> FanatecWheel<Cow<'a, Path>> {
    pub fn new<P: Into<Option<&'a Path>>>(p: P) -> io::Result<Self> {
        Self::try_from(p.into())
    }
}
