use crate::util::PathContainer;
use crate::fs_util::open_write;
use regex::Regex;
use std::{
    borrow::Cow,
    fs,
    io::{
        self,
        ErrorKind,
        Write,
    },
    path::{
        Path,
        PathBuf,
    },
};

#[repr(transparent)]
pub struct FanatecLeds<P: AsRef<Path>> {
    path: P,
}

const fn to_brightness(on: bool) -> u8 {
    if on {
        1
    } else {
        0
    }
}

#[derive(Debug, thiserror::Error)]
enum LedNameError<'a> {
    #[error("Invalid LED filename: {0}")]
    InvalidName(Cow<'a, str>),
}

impl<'a> LedNameError<'a> {
    #[inline(always)]
    fn invalid_name(name: &'a str) -> Self {
        LedNameError::InvalidName(Cow::Borrowed(name))
    }
}

fn led_filename_to_name(filename: &str) -> Result<String, LedNameError<'_>> {
    lazy_static! {
        static ref LED_PATTERN: Regex = Regex::new(r"::([0-9A-Za-z_]+)$").unwrap();
    }
    let captures = LED_PATTERN.captures(filename)
        .map(Ok)
        .unwrap_or_else(|| Err(LedNameError::invalid_name(filename)))?;
    let name = captures.get(1)
        .map(Ok)
        .unwrap_or_else(|| Err(LedNameError::invalid_name(filename)))?;
    Ok(name.as_str().to_string())
}

impl<P: AsRef<Path>> FanatecLeds<P> {
    #[inline(always)]
    pub fn new(path: P) -> Self {
        FanatecLeds { path }
    }

    fn led_path(&self, name: &str) -> io::Result<PathBuf> {
        let pattern = format!("::{}", name);
        let mut led_dir = self.subitem("leds");
        fs::read_dir(&led_dir)?
            .filter_map(Result::ok)
            .map(|entry| entry.file_name())
            .find(|name| name.to_string_lossy().ends_with(&pattern))
            .map(move |name| {
                led_dir.push(name);
                led_dir
            })
            .ok_or_else(|| {
                let msg = format!("No led with name \"{}\" found in path: {}", name, self.path().display());
                io::Error::new(ErrorKind::NotFound, msg)
            })
    }

    fn brightness_path(&self, name: &str) -> io::Result<PathBuf> {
        self.led_path(name).map(|mut p| {
            p.push("brightness");
            p
        })
    }

    pub fn set_led(&self, name: &str, on: bool) -> io::Result<()> {
        let value: u8 = to_brightness(on);
        let mut f = self.brightness_path(name)
            .and_then(open_write)?;
        write!(&mut f, "{}", value)
    }

    pub fn get_leds(&self) -> io::Result<impl Iterator<Item=String>> {
        let ret = fs::read_dir(self.subitem("leds"))?
            .filter_map(Result::ok)
            .map(|entry| entry.file_name())
            .filter_map(|name| led_filename_to_name(&name.to_string_lossy()).ok());
        Ok(ret)
    }
}

impl<P: AsRef<Path>> PathContainer for FanatecLeds<P> {
    #[inline(always)]
    fn path(&self) -> &Path {
        self.path.as_ref()
    }
}
