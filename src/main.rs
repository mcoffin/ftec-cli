extern crate clap;
extern crate env_logger;
extern crate log;
extern crate thiserror;
extern crate regex;
#[macro_use] extern crate lazy_static;

mod commands;
mod logging;
pub mod range_error;
pub mod ftec;
pub(crate) mod fs_util;
pub(crate) mod util;

use clap::{
    Parser,
    crate_version,
    crate_authors,
};
use log::*;
use std::{
    io,
    process,
    path::{
        Path,
        PathBuf,
    },
    str::FromStr,
    time::Duration,
    thread,
};
use util::PathContainer;

include!("./config.rs");

trait LedsExt {
    fn cycle_led(&self, name: &str, time: Duration) -> io::Result<()>;
}

impl<P: AsRef<Path>> LedsExt for ftec::FanatecLeds<P> {
    #[inline(always)]
    fn cycle_led(&self, name: &str, time: Duration) -> io::Result<()> {
        self.set_led(name, true)?;
        thread::sleep(time);
        self.set_led(name, false)
    }
}

impl Config {
    #[inline(always)]
    fn is_dry_run(&self) -> bool {
        self.dry_run
    }

    fn run(&self) -> io::Result<()> {
        use SubCommand::*;
        if self.is_dry_run() {
            warn!("Skipping actions due to dry run!");
            return Ok(());
        }
        match &self.subcommand {
            SetLoadCell(config) => {
                let pedals = ftec::FanatecPedals::new(self.device_node.as_borrowed())?;
                debug!("pedals path: {}", pedals.path().display());
                pedals.set_load_cell(config.value())?;
                info!("set load cell to: {}%", config.value() * 10);
                Ok(())
            },
            SetDisplayNumber(config) => {
                let wheel = ftec::FanatecWheel::new(self.device_node.as_borrowed())?;
                debug!("wheel path: {}", wheel.path().display());
                wheel.set_display_number(config.value())?;
                info!("set display number to: {}", config.value());
                Ok(())
            },
            CycleLeds(config) => {
                let path: Option<&Path> = self.device_node.as_borrowed().into();
                let wheel = ftec::FanatecWheel::new(path)?;
                let leds = wheel.leds();
                let led_names: Vec<_> = leds.get_leds()
                    .map(|it| {
                        let mut v: Vec<_> = it.collect();
                        v.sort();
                        v
                    })?;
                info!("cycling leds for device: {}", wheel.path().display());
                for name in &led_names {
                    info!("cycling led: {}", &name);
                    leds.cycle_led(name, config.time())?;
                }
                Ok(())
            }
        }
    }
}

fn main() {
    logging::init();
    let config = Config::parse();
    debug!("{:?}", &config);
    if let Err(e) = config.run() {
        error!("{}", &e);
        process::exit(1);
    }
    debug!("done");
}
