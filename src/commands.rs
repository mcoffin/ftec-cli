use crate::{
    ftec::LOAD_CELL_RANGE,
};
use std::{
    str::FromStr,
    time::Duration,
};

#[derive(Debug, clap::Parser)]
pub struct SetDisplayNumber {
    value: u32,
}

impl SetDisplayNumber {
    #[inline(always)]
    pub fn value(&self) -> u32 {
        self.value
    }
}

#[derive(Debug, thiserror::Error)]
enum LoadCellValueParseError {
    #[error("{0}")]
    Parse(<u8 as FromStr>::Err),
    #[error("value {0} is not in range {1:?}")]
    Range(u8, &'static std::ops::Range<u8>),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
struct LoadCellValue(u8);

impl FromStr for LoadCellValue {
    type Err = LoadCellValueParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let v = s.parse::<u8>()
            .map_err(LoadCellValueParseError::Parse)?;
        if LOAD_CELL_RANGE.contains(&v) {
            Ok(LoadCellValue(v))
        } else {
            Err(LoadCellValueParseError::Range(v, &LOAD_CELL_RANGE))
        }
    }
}

#[derive(Debug, clap::Parser)]
pub struct SetLoadCell {
    value: LoadCellValue,
}

impl SetLoadCell {
    #[inline(always)]
    pub fn value(&self) -> u8 {
        self.value.0
    }
}

#[derive(Debug, clap::Parser)]
pub struct CycleLeds {
    #[clap(long = "time", short, default_value = "500")]
    time_ms: u64,
}

impl CycleLeds {
    #[inline(always)]
    pub fn time(&self) -> Duration {
        Duration::from_millis(self.time_ms)
    }
}
