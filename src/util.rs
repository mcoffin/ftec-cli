use std::{
    iter,
    path::{
        Path,
        PathBuf,
    },
};
pub trait PathContainer {
    fn path(&self) -> &Path;

    fn subpath<It: IntoIterator>(&self, it: It) -> PathBuf where
        <It as IntoIterator>::Item: AsRef<Path>,
    {
        let mut ret = PathBuf::from(self.path());
        ret.extend(it);
        ret
    }

    fn subitem<P: AsRef<Path>>(&self, p: P) -> PathBuf {
        self.subpath(iter::once(p))
    }
}
