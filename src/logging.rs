use std::sync::Once;

static INIT_LOGGING: Once = Once::new();

#[cfg(debug_assertions)]
const DEFAULT_LOG_FILTER: &str = concat!(env!("CARGO_CRATE_NAME"), "=", "debug");
#[cfg(not(debug_assertions))]
const DEFAULT_LOG_FILTER: &str = concat!(env!("CARGO_CRATE_NAME"), "=", "info");

pub fn init() {
    INIT_LOGGING.call_once(|| {
        use env_logger::{
            Env,
            Builder,
        };
        let cfg = Env::default()
            .default_filter_or(DEFAULT_LOG_FILTER);
        Builder::from_env(cfg).init();
    });
}
