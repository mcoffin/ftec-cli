use std::{
    io,
    fs,
    path::Path,
};

#[inline(always)]
pub fn open_write<P: AsRef<Path>>(p: P) -> io::Result<fs::File> {
    fs::OpenOptions::new()
        .write(true)
        .open(p)
}
