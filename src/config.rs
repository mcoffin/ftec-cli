#[derive(Debug, clap::Parser)]
#[command(version = crate_version!(), author = crate_authors!())]
pub(crate) struct Config {
    #[arg(long)]
    pub dry_run: bool,
    #[arg(long, short, default_value = "auto")]
    device_node: DeviceNode<PathBuf>,
    #[command(subcommand)]
    subcommand: SubCommand,
}

#[derive(Debug, clap::Subcommand)]
pub(crate) enum SubCommand {
    SetLoadCell(commands::SetLoadCell),
    SetDisplayNumber(commands::SetDisplayNumber),
    CycleLeds(commands::CycleLeds),
}

#[derive(Debug, Clone)]
pub(crate) enum DeviceNode<P: AsRef<Path>> {
    AutoDetect,
    Path(P),
}

impl DeviceNode<PathBuf> {
    #[inline(always)]
    pub fn as_borrowed(&self) -> DeviceNode<&Path> {
        match self {
            DeviceNode::AutoDetect => DeviceNode::AutoDetect,
            DeviceNode::Path(p) => DeviceNode::Path(p.as_ref()),
        }
    }
}

impl<P: AsRef<Path>> From<DeviceNode<P>> for Option<P> {
    #[inline(always)]
    fn from(n: DeviceNode<P>) -> Option<P> {
        match n {
            DeviceNode::Path(v) => Some(v),
            _ => None,
        }
    }
}

impl FromStr for DeviceNode<PathBuf> {
    type Err = io::Error;

    fn from_str(s: &str) -> io::Result<Self> {
        use std::io::ErrorKind;
        match s {
            "auto" => Ok(DeviceNode::AutoDetect),
            p => {
                let p: &Path = p.as_ref();
                if !p.exists() {
                    let msg = format!("No such device node: {}", p.display());
                    return Err(io::Error::new(ErrorKind::NotFound, msg));
                }
                Ok(DeviceNode::Path(p.to_path_buf()))
            },
        }
    }
}
