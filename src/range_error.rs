use std::{
    error::Error,
    fmt::{
        self,
        Debug,
        Display,
    },
    io::{
        self,
        ErrorKind,
    },
    ops::Range,
};

#[derive(Debug)]
pub struct RangeError<Idx, V> {
    range: Range<Idx>,
    value: V,
}

impl<Idx, V> RangeError<Idx, V> where
    Idx: Copy,
    V: Copy,
{
    #[inline(always)]
    pub fn new(range: Range<Idx>, value: V) -> Self {
        RangeError { range, value }
    }
}

impl<Idx, V> Display for RangeError<Idx, V>
where
    Idx: Display,
    V: Display,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Out of range ({} .. {}): {}", &self.range.start, &self.range.end, &self.value)
    }
}

impl<Idx, V> From<RangeError<Idx, V>> for io::Error
where
    Idx: Display,
    V: Display,
{
    fn from(e: RangeError<Idx, V>) -> Self {
        io::Error::new(ErrorKind::Other, e.to_string())
    }
}

impl<Idx, V> Error for RangeError<Idx, V> where
    Idx: Display + Debug,
    V: Display + Debug,
{}
